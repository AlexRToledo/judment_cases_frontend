import HttpClient from '../../core/HttpClient/HttpClient';
class NotificationsService extends HttpClient {
    constructor() {
        super();
        this.url = '/notify'
    }

    async GetCount() {
        try {
            return await this.Get(`${this.url}/count`);
        } catch (err) {
            return null
        }
    }

    async GetList(date) {
        try {
            return await this.Get(`${this.url}?date=${date}`);
        } catch (err) {
            return null
        }
    }
}

export default NotificationsService;