import HttpClient from '../../core/HttpClient/HttpClient';
class FilesService extends HttpClient {
    constructor() {
        super();
        this.url = '/folders';
        this.suburl = 'files';
    }

    async GetList(id) {
        try {
            return await this.Get(`${this.url}/${id}/${this.suburl}`);
        } catch (err) {
            return null
        }
    }

    async Create(data) {
        try {
            return await this.DataWithFile(`${this.url}/${this.suburl}/create`, data);
        } catch (err) {
            return null
        }
    }

    async Edit(id, data) {
        try {
            return await this.Put(`${this.url}/edit/${id}`, data);
        } catch (err) {
            return null
        }
    }

    async Remove(id) {
        try {
            return await this.Delete(`${this.url}/${this.suburl}/remove`, {_id: id})
        } catch (err) {
            return null;
        }
    }

    async GetData(id) {
        try {
            return await this.Get(`${this.url}/${this.suburl}/download/${id}`)
        } catch (err) {
            return null;
        }
    }

    async Download(path) {
        try {
            return await this.Axios.get(this._staticUrl + path, {headers: this.HeadersFile(), responseType: 'blob'});
        } catch (err) {

        }
    }
}

export default FilesService;