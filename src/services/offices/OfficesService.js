import HttpClient from '../../core/HttpClient/HttpClient';
class OfficesService extends HttpClient {
    constructor() {
        super();
        this.url = '/offices'
    }

    async GetList(page=1) {
        try {
            return await this.Get(`${this.url}?page=${page}`);
        } catch (err) {
            return null
        }
    }

    async Create(data) {
        try {
            return await this.Post(`${this.url}/create`, data);
        } catch (err) {
            return null
        }
    }

    async Edit(id, data) {
        try {
            return await this.Put(`${this.url}/edit/${id}`, data);
        } catch (err) {
            return null
        }
    }

    async Remove(id) {
        try {
            return await this.Delete(`${this.url}/remove`, {_id: id})
        } catch (err) {
            return null;
        }
    }

    async GetData() {
        try {
            return await this.Get(`${this.url}/info`)
        } catch (err) {
            return null;
        }
    }
}

export default OfficesService;