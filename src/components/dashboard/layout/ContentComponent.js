import React from 'react';
import { Switch } from 'react-router-dom';
import IComponent from '../../../core/IComponent/IComponent'
import CasesComponent from "../components/Cases/CasesComponent";
import UsersComponent from "../components/Users/UsersComponent";
import ProtectedRoutes from "../../../core/routes/ProtectedRoutes";
import CasesFormComponent from "../components/Cases/CasesFormComponent";
import UsersFormComponent from "../components/Users/UsersFormComponent";
import TopMenuComponent from "../layout/TopMenuComponent";
import CasesViewComponent from "../components/Cases/CasesViewComponent";
import CalendarComponent from "../components/Calendar/CalendarComponent";
import OfficesComponent from "../components/Office/OfficesComponent";
import OfficesFormComponent from "../components/Office/OfficesFormComponent";
import InfoComponent from "../components/Info/InfoComponent";
import ProfileComponent from "../components/Profile/ProfileComponent";


class ContentComponent extends IComponent{
    render() {
        return (
            <section id={'content'} className={'uk-width-4-5@m'}>
                <TopMenuComponent/>
                <div className={'content-wrapper'}>
                    <Switch>
                        <ProtectedRoutes exact path="/dashboard" component={InfoComponent}/>
                        <ProtectedRoutes exact path="/dashboard/cases" component={CasesComponent}/>
                        <ProtectedRoutes exact path="/dashboard/cases/create" component={CasesFormComponent}/>
                        <ProtectedRoutes exact path="/dashboard/cases/edit/:id" component={CasesFormComponent}/>
                        <ProtectedRoutes exact path="/dashboard/cases/view/:id" component={CasesViewComponent}/>
                        <ProtectedRoutes exact path="/dashboard/calendar" component={CalendarComponent}/>
                        <ProtectedRoutes exact path="/dashboard/users" component={UsersComponent}/>
                        <ProtectedRoutes exact path="/dashboard/users/create" component={UsersFormComponent}/>
                        <ProtectedRoutes exact path="/dashboard/users/edit/:id" component={UsersFormComponent}/>
                        <ProtectedRoutes exact path="/dashboard/offices" component={OfficesComponent}/>
                        <ProtectedRoutes exact path="/dashboard/offices/edit/:id" component={OfficesFormComponent}/>
                        <ProtectedRoutes exact path="/dashboard/profile" component={ProfileComponent}/>
                    </Switch>
                </div>
            </section>
        );
    }
}

export default ContentComponent;