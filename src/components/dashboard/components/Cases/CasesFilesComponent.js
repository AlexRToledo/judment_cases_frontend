import React from 'react';
import IComponent from "../../../../core/IComponent/IComponent";
import FormComponent from "../../../../core/Components/FormComponent";
import ModalComponent from "../../../../core/Components/ModalComponent";
import FoldersService from "../../../../services/folders/FoldersService";
import FilesService from "../../../../services/files/FilesService";
import UIkit from 'uikit';

const service = new FoldersService();

class CasesFilesComponent extends IComponent {
    constructor() {
        super();
        this.filesService = new FilesService();
        this.state = {
            active: null,
            name: '',
            notes: '',
            nameUpdated: '',
            data: [],
            selected: null,
            file: {},
            foldersfiles: [],
            selected_file: ''
        };
    }

    async List() {
        let res = await service.GetList(this.props.case_number);
        if (res && !res.error) {
            this.setState({
                data: res.data.folders
            })
        }
    }

    async componentDidMount() {
        try {
            await this.List()
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    verifyActive(index) {
        return this.state.active === index;
    }

    async onSubmitFolder(e) {
        try {
            e.preventDefault();
            if(e.target.checkValidity()) {
                let res = await service.Create({
                    name: this.state.name,
                    case_number: this.props.case_number,
                });
                if (res && !res.error) {
                    await this.List();
                }
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    handlerSetFieldsFolder() {
        return (
            <div className={'uk-grid'}>
                <div className="uk-margin uk-width-1-2@s">
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-stacked-text" type="text" name={'name'} value={this.state.name} placeholder="Nombre de la Carpeta..." onChange={this.handleFields.bind(this)} required/>
                    </div>
                </div>
                <div className={'uk-width-1-2@s'}>
                    <button className={'uk-button uk-button-primary '} type='submit'>
                        <span>Crear</span>
                    </button>
                </div>
            </div>
        )
    }

    handlerSetFieldsUpdateFolderNameModal() {
        return (
            <div>
                <div className={'uk-grid'}>
                    <div className="uk-margin uk-width-1-1@s">
                        <div className="uk-form-controls">
                            <input className="uk-input" id="form-stacked-text" type="text" name={'nameUpdated'} value={this.state.nameUpdated} placeholder="Nombre de la Carpeta..." onChange={this.handleFields.bind(this)} required/>
                        </div>
                    </div>
                </div>
                <div className={'uk-flex uk-flex-right'}>
                    <button className={'uk-button uk-button-primary uk-margin-right'} type='submit'>
                        <span>Guardar</span>
                    </button>
                    <button className="uk-button uk-button-default uk-modal-close" type="button">Cancelar</button>
                </div>
            </div>
        )
    }

    async GetFiles() {
        try {
            const res  = await this.filesService.GetList(this.state.selected._id);
            if(res && !res.error) {
                this.setState({
                    foldersfiles: res.data.files
                });
            } else {
                return [];
            }
        } catch (err) {
            this.notify('Ha ocurrido un error')
        }
    }

    handlerFileModalEraseConfirm(id) {
        this.setState({
            selected_file: id
        });
    }

    CleanSelectedFile() {
        this.setState({
            selected_file: ''
        })
    }

    handlerSetListViewFolderModal() {
        if(this.state.foldersfiles.length <= 0) {
            return (
                <div className={'uk-grid'}>
                    <p>No hay contenido en este momento.</p>
                </div>
            )
        } else {
            return (<div className={'uk-grid files-modal'}>

                        {this.state.selected_file === '' ? (
                            <div>
                                <table className={'uk-table'}>
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Nombre</th>
                                            <th>Notas</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.state.foldersfiles.map((ele, index) => {
                                        return <tr key={index}>
                                            <td>
                                                {   ele.note && ele.note !== "undefined" ? (
                                                    <span className={'uk-icon-button'} uk-icon="icon: info" uk-tooltip={'title:'+ ele.note + '; pos: left'}></span>
                                                ) : (
                                                    <span className={'uk-icon-button'} uk-icon="icon: info" uk-tooltip={'title: No posee nota; pos: left'}></span>
                                                )}
                                                <span className={'uk-icon-button'} uk-icon="icon: trash" uk-tooltip="title: Eliminar; pos: top" uk-toggle="target: #confirmFile" onClick={() => this.handlerFileModalEraseConfirm(ele._id)}></span>
                                                <span className={'uk-icon-button uk-margin-right'} uk-icon="icon: download" uk-tooltip="title: Descargar; pos: top" onClick={() => this.handlerDownloadFile(ele._id)}></span>
                                            </td>
                                            <td className={'field-table crop-field'}>
                                                <span>{ele.name}</span>
                                            </td>
                                            <td>
                                                { ele.note && ele.note !== "undefined" ? (
                                                    <div className={'crop-field'}>
                                                        <span uk-tooltip={'title:'+ ele.note + '; pos: right'}>{ele.note}</span>
                                                    </div>
                                                ) : (
                                                    <div className={'crop-field'}>No posee Nota</div>
                                                )}
                                            </td>
                                            <td className={'crop-field'}>
                                                <span>{ new Date(ele.createdAt).toLocaleDateString()}</span>
                                            </td>
                                        </tr>
                                    })}
                                    </tbody>
                                </table>
                            </div>
                        ): (
                            <div>
                                <h5>Esta seguro que desea eliminar este archivo?</h5>
                                <p className="uk-text-right">
                                    <button className="uk-button uk-button-primary uk-margin-small-right" type="button" onClick={() => this.handlerModalConfirmActionFile()}>Aceptar</button>
                                    <button className="uk-button uk-button-default" type="button" onClick={() => this.CleanSelectedFile()}>Cancelar</button>
                                </p>
                            </div>
                        )}
                    </div>
            )
        }
    }

    async handlerDownloadFile(id) {
        try {
            this.notify('Descargando archivo.');
            let res = await this.filesService.GetData(id);
            if(res && !res.error) {
                let file = await this.filesService.Download(`${res.data.path}`);
                await this.FileDownload(file, res.data.name);
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    ExtensionParser(extension) {
        switch (extension) {
            case 'png':
            case 'jpg':
                return 'Imagen';
            default:
                return 'Documento';
        }
    }

    async FileDownload(file, name) {
        const url = window.URL.createObjectURL(new Blob([file.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', name);
        document.body.appendChild(link);
        link.click();
    }

    handlerModalUploadFileAction() {
        return(
            <div>
                <div className={'uk-margin'}>
                    <label className="uk-form-label" htmlFor="form-stacked-text">Nombre</label>
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-stacked-text" type="text" name={'name'} placeholder="Nombre del Archivo..." onChange={this.handleFields.bind(this)} required/>
                    </div>
                </div>
                <div className={'uk-margin'}>
                    <label className="uk-form-label" htmlFor="form-stacked-text">Archivo</label>
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-stacked-text" type="file" name={'file'} placeholder="Archivo del Expediente..." onChange={this.handleFields.bind(this)} required/>
                    </div>
                </div>
                <div className={'uk-margin'}>
                    <label className="uk-form-label" htmlFor="form-stacked-text">Notas</label>
                    <div className="uk-form-controls">
                        <textarea className="uk-textarea" name={'note'} placeholder="Notas del Archivo..." onChange={this.handleFields.bind(this)}></textarea>
                    </div>
                </div>
                <div className={'uk-flex uk-flex-right'}>
                    <button className={'uk-button uk-button-primary uk-margin-right'} type='submit'>
                        <span>Guardar</span>
                    </button>
                    <button className="uk-button uk-button-default uk-modal-close" type="button">Cancelar</button>
                </div>
            </div>
        )
    }

    async handlerSubmitFile(e) {
        try {
            let res;
            e.preventDefault();
            if(e.target.checkValidity()) {
                const data = new FormData();
                data.append('case_jud', this.props.case_number);
                data.append('folder_id', this.state.selected._id);
                data.append('name', this.state.name);
                data.append('note', this.state.note);
                data.append('file', this.state.file, this.state.file.name);
                res = await this.filesService.Create(data);
                if (res && !res.error) {
                    UIkit.modal(document.getElementById('upload')).hide();
                    await this.GetFiles()
                }
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async handlerSelectFolder(element) {
        await this.setState({
            selected: element
        });
        await this.GetFiles();
    }

    async handlerModalConfirmAction() {
        try {
            const res = await service.Remove(this.state.selected._id);
            if(res && !res.error) {
                await this.List()
            }
            this.notify(res.message)
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async handlerModalConfirmActionFile() {
        try {
            const res = await this.filesService.Remove(this.state.selected_file);
            if(res && !res.error) {
                this.CleanSelectedFile();
                await this.GetFiles()
            }
            this.notify(res.message)
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async handlerModalUpdateAction(e) {
        try {
            e.preventDefault();
            if(e.target.checkValidity()) {
                const res = await service.Edit(this.state.selected._id, {name: this.state.nameUpdated});
                if(res && !res.error) {
                    this.setState({
                        nameUpdated: ''
                    });
                    await this.List();
                }
                UIkit.modal(document.getElementById('update')).hide();
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    render() {
        return (
            <section id={'caseFiles'}>
                <div className={'uk-card uk-card-default uk-card-body'}>
                    <h1 className={'title'}>Documentos</h1>
                    <div className={"uk-grid"} >
                        <div className={'uk-width-1-1@s'}>
                            <FormComponent handlerSubmit={this.onSubmitFolder.bind(this)} nocard={false}>
                                {this.handlerSetFieldsFolder()}
                            </FormComponent>
                        </div>
                        <div className={'uk-width-1-4@s folders'}>
                            <h3>Carpetas</h3>
                            <ul className={'uk-nav ul-folders uk-flex uk-flex-column uk-flex-center'}>
                                {this.state.data.map((element, elementIndex) => {
                                    return (
                                        <li key={elementIndex} className={'uk-flex uk-flex-middle uk-padding-small'}>
                                            <div className={ 'top-actions' }>
                                                <a type="button"><span className={ 'uk-icon-button' } uk-icon="menu"></span></a>
                                                <div className={ 'uk-dropdown' } uk-dropdown="pos: left-center">
                                                    <ul className="uk-nav uk-dropdown-nav">
                                                        <li className={'uk-nav-item uk-padding-small'}><span uk-tooltip="title: Agregar documento; pos: bottom" uk-toggle="target: #upload" onClick={() => this.handlerSelectFolder(element)}>Agregar</span></li>
                                                        <li className={'uk-nav-item uk-padding-small'}><span uk-tooltip="title: Editar; pos: bottom" uk-toggle="target: #update" onClick={() => this.handlerSelectFolder(element)}>Cambiar Nombre</span></li>
                                                        <li className={'uk-nav-item uk-padding-small'}><span uk-tooltip="title: Eliminar; pos: bottom" uk-toggle="target: #confirm" onClick={() => this.handlerSelectFolder(element)}>Eliminar</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <span className={'uk-nav-item ' + (this.verifyActive(elementIndex) ? 'uk-active' : '') + 'crop-field uk-margin-left'} onClick={() => this.handlerSelectFolder(element)}>{element.name}</span>
                                        </li>
                                    )
                                })}
                            </ul>
                        </div>
                        <div className={'uk-width-3-4@s files-folders'}>
                            <h3>{(this.state.selected !== null ? this.state.selected.name : '')}</h3>
                            <div>
                                {this.handlerSetListViewFolderModal()}
                            </div>
                        </div>
                    </div>
                </div>
                <ModalComponent title={'Subir Archivo: '} modalID={'upload'} actions={false}>
                    <FormComponent nocard={false} handlerSubmit={this.handlerSubmitFile.bind(this)}>
                        {this.handlerModalUploadFileAction()}
                    </FormComponent>
                </ModalComponent>
                <ModalComponent title={'Editar Carpeta'} modalID={'update'} actions={false}>
                    <FormComponent nocard={false} handlerSubmit={this.handlerModalUpdateAction.bind(this)}>
                        {this.handlerSetFieldsUpdateFolderNameModal()}
                    </FormComponent>
                </ModalComponent>
                <ModalComponent title={'Esta seguro que desea eliminar esta carpeta?'} modalID={'confirm'} actions={true} handlerModalAction={this.handlerModalConfirmAction.bind(this)}/>
            </section>
        )
    }
}

export default CasesFilesComponent;