import React from 'react';
import IComponent from "../../../../core/IComponent/IComponent";
import CasesService from '../../../../services/cases/CasesService';
import CasesFilesComponent from "./CasesFilesComponent";

const service = new CasesService();

class CasesViewComponent extends IComponent{
    constructor() {
        super();
        this.state = {
            id: '',
            name: '',
            case_number: '',
            status: 'false',
            involved: '',
            court: '',
            year: new Date(),
            courts: [
                "TRIBUNAL SUPERIOR DE JUSTICIA DEL ESTADO DE YUCATAN.",
                "JUZGADO PRIMERO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO SEGUNDO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO TERCERO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO DE ORALIDAD MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO SEGUNDO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO TERCERO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO CUARTO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL  ESTADO",
                "JUZGADO PRIMERO DE LO FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                "JUZGADO SEGUNDO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                "JUZGADO SEGUNDO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                "JUZGADO TERCERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                "JUZGADO TERCERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                "JUZGADO CUARTO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO QUINTO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO SEXTO EN MATERIA DE ORALIDAD FAMILIAR DEL PODER JUDICIAL DEL ESTADO.",
                "JUZGADO SEPTIMO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                "JUZGADO PRIMERO DE EJECUCION DE SENTENCIA DEL ESTADO DE YUCATAN.",
                "JUZGADO SEGUNDO DE EJECUCION DE SENTENCIA EN MATERIA PENAL DEL ESTADO.",
                "JUZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN PROGRESO, YUCATAN.",
                "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN UMAN, YUCATAN.",
                "JUZGADO TERCERO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN MOTUL, YUCATAN.",
                "JUZGADO CUARTO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN KANASIN, YUCATAN",
                "JUZGADO QUINTO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN IZAMAL, YUCATAN.",
                "UZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL SEGUNDO DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TEKAX, YUCATAN.",
                "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL SEGUNDO DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TICUL, YUCATAN.",
                "JUZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL TERCER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN VALLADOLID, YUCATAN.",
                "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL TERCER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TIZIMIN, YUCATAN."
            ],
            users_follow: []
        };
    }

    async componentDidMount() {
        try {
            const { match: {params} } = this.props;

            if(params.id) {
                const res = await service.GetData(params.id);
                if(res && !res.error) {
                    this.setState({
                        name: res.data.case.name,
                        case_number: res.data.case.case_number,
                        involved: res.data.case.involved,
                        court: res.data.case.court,
                        formType: 'edit',
                        id: params.id,
                        status: JSON.stringify(res.data.case.status),
                        title: 'Ver',
                        users_follow: res.data.case.users_follow
                    })
                } else {
                    this.notify(res.message)
                }
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    handlerSetFields() {
        try {
            return (
                <div className={'uk-card uk-card-default uk-card-body'}>
                    <ul className={'uk-list'}>
                        <li>
                            <div key={1}><strong>Nombre</strong>: {this.state.name}</div>
                        </li>
                        <li>
                            <div key={2}><strong>Expediente</strong>: {this.state.case_number}</div>
                        </li>
                        <li>
                            <div key={3}><strong>Estado</strong>: {this.state.status === true ? 'Cerrado' : 'Abierto'}</div>
                        </li>
                        <li>
                            <div key={4}><strong>Juzgado</strong>: {this.state.court}</div>
                        </li>
                        <li>
                            <div key={5}><strong>Involucrados</strong>: {this.state.involved}</div>
                        </li>
                        <li>
                            <div key={5}><strong>Autorizados</strong>: {
                                this.state.users_follow.map((ele, key) => {
                                    return (
                                            <span>{ele.username}&nbsp;</span>
                                    )
                                })
                            }</div>
                        </li>
                    </ul>
                </div>
            )
        } catch (err) {

        }
    }

    render() {
        return (
            <section>
                <div className={'content-wrapper'}>
                    <h1 className={'title'}>{this.state.title} Expediente</h1>
                    <div className={"uk-grid uk-flex uk-flex-center"} >
                        <div className={'uk-width-1-4@s'}>
                            {this.handlerSetFields()}
                        </div>
                        <div className={'uk-width-3-4@s'}>
                            {this.state.id !== "" &&
                                <CasesFilesComponent case_number={this.state.id}/>
                            }
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default CasesViewComponent;