import React from 'react';
import IComponent from "../../../../core/IComponent/IComponent";
import CasesService from '../../../../services/cases/CasesService';
import PaginationComponent from "../../../../core/Components/PaginationComponent";
import CardListComponent from "../../../../core/Components/CardListComponent";
import AddButton from "../../../../core/Components/IconNav/AddButton";
import SearchFormComponent from "../../../../core/Components/SearchFormComponent";

class CasesComponent extends IComponent {
    constructor () {
        super();
        this.service = new CasesService();
        this.state = {
            data: [],
            keys: [
                { field: 'case_number', label: 'Expediente' },
                { field: 'status', label: 'Estado', options: { true: 'Cerrado', false: 'Abierto' } },
            ],
            keys2: [
                { field: 'court', label: 'Seleccione Juzgado', options: [
                        "TRIBUNAL SUPERIOR DE JUSTICIA DEL ESTADO DE YUCATAN.",
                        "JUZGADO PRIMERO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO SEGUNDO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO TERCERO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO PRIMERO DE ORALIDAD MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO PRIMERO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO SEGUNDO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO TERCERO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO CUARTO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL  ESTADO",
                        "JUZGADO PRIMERO DE LO FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO PRIMERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO PRIMERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                        "JUZGADO SEGUNDO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                        "JUZGADO SEGUNDO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                        "JUZGADO TERCERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                        "JUZGADO TERCERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                        "JUZGADO CUARTO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO QUINTO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                        "JUZGADO SEXTO EN MATERIA DE ORALIDAD FAMILIAR DEL PODER JUDICIAL DEL ESTADO.",
                        "JUZGADO SEPTIMO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                        "JUZGADO PRIMERO DE EJECUCION DE SENTENCIA DEL ESTADO DE YUCATAN.",
                        "JUZGADO SEGUNDO DE EJECUCION DE SENTENCIA EN MATERIA PENAL DEL ESTADO.",
                        "JUZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN PROGRESO, YUCATAN.",
                        "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN UMAN, YUCATAN.",
                        "JUZGADO TERCERO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN MOTUL, YUCATAN.",
                        "JUZGADO CUARTO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN KANASIN, YUCATAN",
                        "JUZGADO QUINTO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN IZAMAL, YUCATAN.",
                        "UZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL SEGUNDO DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TEKAX, YUCATAN.",
                        "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL SEGUNDO DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TICUL, YUCATAN.",
                        "JUZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL TERCER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN VALLADOLID, YUCATAN.",
                        "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL TERCER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TIZIMIN, YUCATAN."
                    ]
                },
            ],
            keys3: [
                { field: 'case_number', label: 'Expediente' },
                { field: 'status', label: 'Estado', options: { true: 'Cerrado', false: 'Abierto' } },
                { field: 'court', label: 'Juzgado' },
            ],
            title: [
                { field: 'name', label: 'Nombre' },
            ],
            params: null,
        };

        this.changePageLoadData = this.changePageLoadData.bind(this);
        this.handlerRemove = this.handlerRemove.bind(this);
        this.handlerSetEditPage = this.handlerSetEditPage.bind(this);
        this.handlerSetViewPage = this.handlerSetViewPage.bind(this);
        this.handlerSearch = this.handlerSearch.bind(this);
    }

    async componentDidMount() {
        try {
            const res = await this.service.GetList();
            if (res && !res.error) {
                this.setState({ data: res.data });
            } else {
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async changePageLoadData(page = 1) {
        try {
            const res = await this.service.GetList(page, this.state.params);
            if (res && !res.error) {
                this.setState({
                    data: res.data
                });
            } else {
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async handlerRemove(id) {
        try {
            const res = await this.service.Remove(id);
            if (res && !res.error) {
                this.notify(res.message);
                this.changePageLoadData();
            } else {
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    handlerSetEditPage(id) {
        this.props.history.push(`/dashboard/cases/edit/${id}`)
    }

    handlerSetViewPage(id) {
        this.props.history.push(`/dashboard/cases/view/${id}`)
    }

    async handlerSearch(params) {
        try {
            if(params.court) {
                params.court = this.state.keys2[0].options[params.court]
            }
            await this.setState({ params: params });
            await this.changePageLoadData();
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    render() {
        return (
            <section>
                <div className={ 'content-wrapper' }>
                    <h1 className={ 'title' }>Listado de Expedientes</h1>
                    <SearchFormComponent title={'Juzgados'} keys={ this.state.keys2 } classname={true} setSearch={ this.handlerSearch }></SearchFormComponent>
                    <div className={ "uk-grid" } >
                        <div className={ 'uk-width-4-5@m' }>
                            <PaginationComponent total={ this.state.data.total } page={ this.state.data.page } pages={ Math.ceil(this.state.data.total / this.state.data.limit) } setPage={ this.changePageLoadData } />
                            <CardListComponent data={ this.state.data.cases } data_keys={ this.state.keys3 } data_title={ this.state.title } width={ 'uk-width-1-3@l uk-width-1-2@m uk-width-1-1@s ' } card_menu={ 'nav' } viewCardClick={true} onRemove={ this.handlerRemove } onSetEditPage={ this.handlerSetEditPage } onSetViewPage={ this.handlerSetViewPage } />
                            {/*<PaginationComponent total={this.state.data.total} page={this.state.data.page} pages={ Math.ceil(this.state.data.total / this.state.data.limit)} setPage={this.changePageLoadData}/>*/ }
                        </div>
                        <div className={ 'uk-width-1-5@m side-actions' }>
                            <AddButton title={ 'Nuevo' } link={ '/dashboard/cases/create' } />
                            <hr />
                            <SearchFormComponent keys={ this.state.keys } setSearch={ this.handlerSearch }></SearchFormComponent>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default CasesComponent;