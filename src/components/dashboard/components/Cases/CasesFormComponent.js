import React from 'react';
import {Link} from 'react-router-dom';
import IComponent from "../../../../core/IComponent/IComponent";
import CasesService from '../../../../services/cases/CasesService';
import UsersService from '../../../../services/users/UsersService';
import FormComponent from "../../../../core/Components/FormComponent";
import DatePicker from "react-datepicker";
import Select from 'react-select';
import Storage from '../../../../helpers/Storage';

const service = new CasesService();
const userservice = new UsersService();
const store = new Storage();

class CasesFormComponent extends IComponent{
    constructor() {
        super();
        this.state = {
            id: '',
            name: '',
            case_number: '',
            status: 'false',
            involved: '',
            court: '',
            year: new Date(),
            title: 'Nuevo',
            formType: 'create',
            courts: [
                "TRIBUNAL SUPERIOR DE JUSTICIA DEL ESTADO DE YUCATAN.",
                "JUZGADO PRIMERO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO SEGUNDO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO TERCERO MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO DE ORALIDAD MERCANTIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO SEGUNDO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO TERCERO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO CUARTO CIVIL DEL PRIMER DEPARTAMENTO JUDICIAL DEL  ESTADO",
                "JUZGADO PRIMERO DE LO FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO PRIMERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                "JUZGADO SEGUNDO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                "JUZGADO SEGUNDO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                "JUZGADO TERCERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                "JUZGADO TERCERO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO. TURNO VESPERTINO",
                "JUZGADO CUARTO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO QUINTO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO",
                "JUZGADO SEXTO EN MATERIA DE ORALIDAD FAMILIAR DEL PODER JUDICIAL DEL ESTADO.",
                "JUZGADO SEPTIMO DE ORALIDAD FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO.",
                "JUZGADO PRIMERO DE EJECUCION DE SENTENCIA DEL ESTADO DE YUCATAN.",
                "JUZGADO SEGUNDO DE EJECUCION DE SENTENCIA EN MATERIA PENAL DEL ESTADO.",
                "JUZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN PROGRESO, YUCATAN.",
                "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN UMAN, YUCATAN.",
                "JUZGADO TERCERO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN MOTUL, YUCATAN.",
                "JUZGADO CUARTO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN KANASIN, YUCATAN",
                "JUZGADO QUINTO MIXTO DE LO CIVIL Y FAMILIAR DEL PRIMER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN IZAMAL, YUCATAN.",
                "UZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL SEGUNDO DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TEKAX, YUCATAN.",
                "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL SEGUNDO DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TICUL, YUCATAN.",
                "JUZGADO PRIMERO MIXTO DE LO CIVIL Y FAMILIAR DEL TERCER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN VALLADOLID, YUCATAN.",
                "JUZGADO SEGUNDO MIXTO DE LO CIVIL Y FAMILIAR DEL TERCER DEPARTAMENTO JUDICIAL DEL ESTADO, CON SEDE EN TIZIMIN, YUCATAN."
            ],
            users_options: [],
            users_follow: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handlerChosenChange = this.handlerChosenChange.bind(this);
    }

    async componentDidMount() {
        try {
            const { match: {params} } = this.props;
            const resuser = await userservice.GetList();
            if(resuser && !resuser.error) {
                let aux = [];
                for(const user of resuser.data.users) {
                    aux.push({ value: user._id, label: user.username },);
                }
                this.setState({
                    users_options: aux
                })
            } else {
                this.notify(resuser.message)
            }
            if(params.id) {
                const res = await service.GetData(params.id);
                if(res && !res.error) {
                    this.setState({
                        name: res.data.case.name,
                        case_number: res.data.case.case_number,
                        involved: res.data.case.involved,
                        court: res.data.case.court,
                        formType: 'edit',
                        id: params.id,
                        status: JSON.stringify(res.data.case.status),
                        title: 'Editar'
                    });
                    let auxedit = [];
                    for(const user of res.data.case.users_follow) {
                        auxedit.push({ value: user._id, label: user.username },);
                    }
                    this.setState({
                        users_follow: auxedit
                    })

                } else {
                    this.notify(res.message)
                }
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    handleChange(date) {
        this.setState({
            year: date
        });
    }

    handlerChosenChange(value, values) {
        this.logChange('New value:', value, 'Values:', values);
        this.setState({ users_follow: value });
    }

    logChange() {
        console.log.apply(console, [].concat(['Select value changed:'], Array.prototype.slice.apply(arguments)));
    }

    handlerSetFields() {
        return (
            <div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Nombre</label>
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-stacked-text" type="text" name={'name'} value={this.state.name} placeholder="Nombre del Expediente..." onChange={this.handleFields.bind(this)} required/>
                    </div>
                </div>
                <div className={'uk-grid uk-margin'}>
                    <div className={'uk-width-1-2@s'}>
                        <label className="uk-form-label" htmlFor="form-stacked-text">Expediente</label>
                        <div className="uk-form-controls">
                            <input className="uk-input" id="form-stacked-text" type="text" name={'case_number'} value={this.state.case_number} min={0} placeholder="Numero de Expediente..." onChange={this.handleFields.bind(this)} required/>
                        </div>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Juzgado</label>
                    <div className="uk-form-controls">
                        <select className="uk-select" id="form-stacked-select" name={'court'} value={this.state.court} onChange={this.handleFields.bind(this)} required>
                            <option value={''} disabled={true}>Seleccione</option>
                            {this.state.courts.map((el, i) =>
                                 <option key={i} value={el}>{el}</option>
                            )}
                        </select>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Partes del Juicio</label>
                    <div className="uk-form-controls">
                        <input className="uk-input" id="form-stacked-text" type="text" name={'involved'} value={this.state.involved} placeholder="Personas..." onChange={this.handleFields.bind(this)} required/>
                    </div>
                </div>
                { store.GetUser().perm === 'isAdmin' &&
                    <div className={'uk-margin'}>
                        <label className="uk-form-label" htmlFor="form-stacked-text">Autorizados</label>
                        <Select
                            isMulti={true}
                            name={'users_follow'}
                            value={this.state.users_follow}
                            options={this.state.users_options}
                            onChange={this.handlerChosenChange}
                        />
                    </div>
                }
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Estado</label>
                    <div className="uk-form-controls uk-form-controls-text">
                        <label className={'uk-margin-small-right'}><input className="uk-radio" type="radio" name="status" value={'false'} onChange={this.handleFields.bind(this)} checked={this.state.status === 'false' ? true : false}/> Abierto</label>
                        <label><input className="uk-radio" type="radio" name="status" value={'true'} onChange={this.handleFields.bind(this)} checked={this.state.status === 'true' ? true : false}/> Cerrado</label>
                    </div>
                </div>
                <div className={'uk-flex uk-flex-right'}>
                    <button className={'uk-button uk-button-primary '} type='submit'>
                        {this.state.formType === 'create' ? (
                           <span>Crear</span>
                        ) : (
                           <span>Editar</span>
                        )}
                    </button>
                    <Link className={'uk-button uk-button-default uk-margin-left'} to="/dashboard/cases">Cancelar</Link>
                </div>
            </div>
        )
    }

    async onSubmit(e) {
        try {
            let res;
            e.preventDefault();
            if(e.target.checkValidity()) {
                let aux = [];
                for(const option of this.state.users_follow) {
                    aux.push(option.value);
                }
                if(this.state.formType === 'create') {
                    res = await service.Create({
                        name: this.state.name,
                        case_number: this.state.case_number,
                        involved: this.state.involved,
                        court: this.state.court,
                        status: this.state.status,
                        year: this.state.year.toLocaleDateString(),
                        users_follow: aux
                    });
                } else {
                    res = await service.Edit(
                    this.state.id,
                    {
                        name: this.state.name,
                        case_number: this.state.case_number,
                        involved: this.state.involved,
                        court: this.state.court,
                        status: this.state.status,
                        year: this.state.year.toLocaleDateString(),
                        users_follow: aux
                    });
                }
                if (res && !res.error) {
                    this.props.history.push('/dashboard/cases');
                }
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    render() {
        return (
            <section>
                <div className={'content-wrapper'}>
                    <h1 className={'title'}>{this.state.title} Expediente</h1>
                    <div className={"uk-grid uk-flex uk-flex-center"} >
                        <div className={'uk-width-1-2@s'}>
                            <FormComponent handlerSubmit={this.onSubmit.bind(this)} >
                                {this.handlerSetFields()}
                            </FormComponent>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default CasesFormComponent;