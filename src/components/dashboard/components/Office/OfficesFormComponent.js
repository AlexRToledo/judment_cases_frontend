import React from 'react';
import ReactDOM from 'react-dom';
import {Link} from 'react-router-dom';
import IComponent from "../../../../core/IComponent/IComponent";
import OfficesService from '../../../../services/offices/OfficesService';
import FormComponent from "../../../../core/Components/FormComponent";
import Json from "../../../../core/utils/Json";

const service = new OfficesService();

class OfficesFormComponent extends IComponent{
    constructor() {
        super();
        this.state = {
            info_plan: {},
            plan: '',
            users: 0,
            admins: 0,
            id: '',
            cases: 0,
            status: 'false',
            address: '',
            formType: 'create',
            total_users: 0,
            total_cases: 0,
            total_admins: 0,
            store: 0
        };
    }

    async componentDidMount() {
        try {
            const { match: {params} } = this.props;

            if(params.id) {
                const res = await service.GetData(params.id);
                if(res && !res.error) {
                    this.setState({
                        plan: res.data.office.plan,
                        users: res.data.office.extra.users,
                        cases: res.data.office.extra.cases,
                        admins: res.data.office.extra.admins,
                        store: res.data.office.extra.store,
                        status: JSON.stringify(res.data.office.status),
                        name: res.data.office.name,
                        address: res.data.office.address,
                        total_users: res.data.total_users,
                        total_cases: res.data.total_cases,
                        total_admins: res.data.total_admins,
                        phone: res.data.office.phone,
                        info_plan: res.data.plan,
                        formType: 'edit',
                        id: params.id
                    });
                } else {
                    this.notify(res.message)
                }
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    handlerSetFields() {
        return (
            <div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Tipo de Plan</label>
                    <div className="uk-form-controls">
                        <select className="uk-select" id="form-stacked-select" name={'plan'} value={this.state.plan} onChange={this.handleFields.bind(this)} required>
                            <option value={''} disabled={true}>Seleccione</option>
                            <option key="1" value="SILVER">PLATA</option>
                            <option key="2" value="GOLD">ORO</option>
                            <option key="3" value="DIAMOND">DIAMANTE</option>
                        </select>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Cantidad de Usuarios +</label>
                    <div className="uk-form-controls">
                        <input ref={'users'} className="uk-input" id="form-stacked-text-name" type="number" name={'users'} value={this.state.users} onChange={this.handleFields.bind(this)} min={0} required/>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Cantidad de Expedientes +</label>
                    <div className="uk-form-controls">
                        <input ref={'cases'} className="uk-input" id="form-stacked-text-email" type="number" name={'cases'} value={this.state.cases} onChange={this.handleFields.bind(this)} min={0} required/>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Cantidad de Administradores +</label>
                    <div className="uk-form-controls">
                        <input ref={'admins'} className="uk-input" id="form-stacked-text-email" type="number" name={'admins'} value={this.state.admins} onChange={this.handleFields.bind(this)} min={0} required/>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Cantidad de Almacenamiento +</label>
                    <div className="uk-form-controls">
                        <input ref={'store'} className="uk-input" id="form-stacked-text-email" type="number" name={'store'} value={this.state.store} onChange={this.handleFields.bind(this)} min={0} required/>
                    </div>
                </div>
                <div className="uk-margin">
                    <label className="uk-form-label" htmlFor="form-stacked-text">Estado</label>
                    <div className="uk-form-controls uk-form-controls-text">
                        <label><input className="uk-radio" type="radio" name="status" value={'true'} onChange={this.handleFields.bind(this)} checked={this.state.status === 'true' ? true : false}/> Activado</label>
                        <label className={'uk-margin-small-left'}><input className="uk-radio" type="radio" name="status" value={'false'} onChange={this.handleFields.bind(this)} checked={this.state.status === 'false' ? true : false}/> Deshabilitado</label>
                    </div>
                </div>
                <div className={'uk-flex uk-flex-right'}>
                    <button className={'uk-button uk-button-primary '} type='submit'>
                        {this.state.formType === 'create' ? (
                            <span>Crear</span>
                        ) : (
                            <span>Editar</span>
                        )}
                    </button>
                    <Link className={'uk-button uk-button-default uk-margin-left'} to="/dashboard/offices">Cancelar</Link>
                </div>
            </div>
        )
    }

    async onSubmit(e) {
        try {
            let res;
            e.preventDefault();
            if(e.target.checkValidity()) {
                let record = {
                    ...this.state
                };
                if(this.state.formType === 'create') {
                    record.password = this.state.password;
                    res = await service.Create(record);
                } else {
                    res = await service.Edit(this.state.id, record);
                }

                if (res && !res.error) {
                    this.props.history.push('/dashboard/offices');
                } else {
                    if(res.field && res.value) {
                        ReactDOM.findDOMNode(this.refs[res.field] + '_input').addClass('uk-form-danger');
                    }
                }
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    showTooltip() {
        let aux = 'INFORMACION DEL PLAN<br>';
        for (let i in this.state.info_plan) {
            aux +=`${i} - ${this.state.info_plan[i]}<br> `;
        }
        return aux;
    }

    render() {
        return (
            <section>
                <div className={'content-wrapper'}>
                    <h1 className={'title'}>Editar Despacho</h1>
                    <div className={"uk-grid uk-flex uk-flex-center"} >
                        <div className={'uk-width-1-2@s'}>
                            <div className={'uk-card uk-card-default uk-card-body'}>
                                <div className={'uk-flex uk-flex-between'}>
                                    <h2>{this.state.name}</h2>
                                    <a href="#" uk-icon="icon: info" uk-tooltip={this.showTooltip()}></a>
                                </div>
                                <p className={'uk-article-meta'}>{this.state.address}</p>
                                <ul className={'uk-list'}>
                                    <li key={'phone'}><strong>Telefono:</strong> {this.state.phone}</li>
                                    <li key={'total_users'}><strong>Total de Usuarios:</strong> {this.state.total_users}</li>
                                    <li key={'total_cases'}><strong>Total de Expedientes:</strong> {this.state.total_cases}</li>
                                    <li key={'total_adminss'}><strong>Total de Administradores:</strong> {this.state.total_admins}</li>
                                </ul>
                            </div>
                        </div>
                        <div className={'uk-width-1-2@s'}>
                            <FormComponent handlerSubmit={this.onSubmit.bind(this)} >
                                {this.handlerSetFields()}
                            </FormComponent>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default OfficesFormComponent;