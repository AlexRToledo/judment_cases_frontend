import React from 'react';
import OfficesService from '../../../../services/offices/OfficesService';
import PaginationComponent from "../../../../core/Components/PaginationComponent";
import IComponent from "../../../../core/IComponent/IComponent";
import SearchFormComponent from "../../../../core/Components/SearchFormComponent";
import TableListComponent from "../../../../core/Components/TableListComponent";
const service = new OfficesService();


class UsersComponent extends IComponent{
    constructor() {
        super();
        this.state = {
            data: [],
            keys: [
                {field: 'name', label: 'Nombre'},
                {field: 'plan', label: 'Plan'},
                {field: 'createdAt', label: 'Creado', 'date': true}
            ]
        };

        this.changePageLoadData = this.changePageLoadData.bind(this);
        this.handlerRemove = this.handlerRemove.bind(this);
        this.handlerSetEditPage = this.handlerSetEditPage.bind(this);
    }

    async componentDidMount() {
        try {
            const res = await service.GetList();
            if(res && !res.error) {
                this.setState({data: res.data});
            } else {
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async changePageLoadData(page=1) {
        try {
            const res = await service.GetList(page);
            if(res && !res.error) {
                this.setState({
                    data: res.data
                });
            } else {
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    async handlerRemove(id) {
        try {
            const res = await service.Remove(id);
            if(res && !res.error) {
                this.notify(res.message);
                this.changePageLoadData();
            } else {
                this.notify(res.message)
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    handlerSetEditPage(id) {
        this.props.history.push(`/dashboard/offices/edit/${id}`)
    }

    render() {
        return (
            <section>
                <div className={'content-wrapper'}>
                    <h1 className={'title'}>Listado de Despachos</h1>
                    <div className={"uk-grid"} >
                        <div className={'uk-width-4-5@m'}>
                            <PaginationComponent total={this.state.data.total} page={this.state.data.page} pages={ Math.ceil(this.state.data.total / this.state.data.limit)} setPage={this.changePageLoadData}/>
                            <TableListComponent data={this.state.data.users} data_keys={this.state.keys} data_title={this.state.title} width={'uk-width-1-3@l uk-width-1-2@m uk-width-1-1@s '} card_menu={'nav'} onRemove={this.handlerRemove} onSetEditPage={this.handlerSetEditPage} table_config={'uk-table-middle uk-table-hover uk-table-striped'}/>
                            {/*<PaginationComponent total={this.state.data.total} page={this.state.data.page} pages={ Math.ceil(this.state.data.total / this.state.data.limit)} setPage={this.changePageLoadData}/>*/}
                        </div>
                        <div className={'uk-width-1-5@m side-actions'}>
                            <SearchFormComponent keys={this.state.keys}></SearchFormComponent>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default UsersComponent;