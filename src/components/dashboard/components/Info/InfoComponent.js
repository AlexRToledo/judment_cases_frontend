import React from 'react';
import {Link} from 'react-router-dom';
import IComponent from "../../../../core/IComponent/IComponent";
import OfficesService from "../../../../services/offices/OfficesService";

class InfoComponent extends IComponent {
    constructor() {
        super();
        this.state = {
            count_users: 0,
            count_admins: 0,
            count_size: 0,
            count_cases: 0,
            plan: {}
        };
        this.service = new OfficesService();
    }

    async componentDidMount() {
        try {
            const res = await this.service.GetData();
            if(res && !res.error) {
                this.setState({
                    count_users: res.data.total_users,
                    count_admins: res.data.total_admins,
                    count_size: res.data.total_size[0] ? res.data.total_size[0].amount : 0,
                    count_cases: res.data.total_cases,
                    plan: res.data.plan
                })
            }
        } catch (err) {
            this.notify('Ha ocurrido un error.')
        }
    }

    render() {
        return (
            <div id={'info'}>
                <div className={'uk-flex uk-flex-center uk-flex-around uk-flex-wrap uk-grid p-side-3'}>
                    <Link to={'/dashboard/users'} className={'uk-width-1-4@l uk-width-1-3@m uk-width-1-1@s '}>
                        <div className={'uk-card uk-card-default uk-card-small uk-card-body'}>
                            <div className={'uk-flex uk-flex-between'}>
                                <h4>Usuarios</h4>
                                <span>{this.state.count_users}/{this.state.plan.max_users}</span>
                            </div>
                            <hr/>
                            <div className={'uk-flex uk-flex-between'}>
                                <h4>Administradores</h4>
                                <span>{this.state.count_admins}/{this.state.plan.max_admins}</span>
                            </div>
                        </div>
                    </Link>
                    <Link to={'/dashboard/cases'} className={'uk-width-1-4@l uk-width-1-3@m uk-width-1-1@s '}>
                        <div className={'uk-card uk-card-default uk-card-small uk-card-body'}>
                            <div className={'uk-flex uk-flex-between'}>
                                <h4>Expedientes</h4>
                                <span>{this.state.count_cases}/{this.state.plan.max_cases}</span>
                            </div>
                            <hr/>
                            <div className={'uk-flex uk-flex-between'}>
                                <h4>Capacidad</h4>
                                <span>{Math.round(this.state.count_size * 1000) / 1000}/{this.state.plan.max_space} GB</span>
                            </div>
                        </div>
                    </Link>
                </div>
            </div>
        );
    }
}

export default InfoComponent;