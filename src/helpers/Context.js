import React from 'react';

export const ThemeContext = React.createContext({
    colors: {},
    setColors: () => {}
});